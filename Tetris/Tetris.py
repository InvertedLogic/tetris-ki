from random import choice
import time 
import numpy as np


# define board size
BOARD_WIDTH = 10
BOARD_HEIGHT = 22

# define Patterns
IPattern = [[0,0,0,0],
			[1,1,1,1]]

TPattern = [[0,1,0,0],
			[1,1,1,0]]

OPattern = [[0,1,1,0],
			[0,1,1,0]]

LPattern = [[0,0,1,0],
			[1,1,1,0]]

JPattern = [[1,0,0,0],
			[1,1,1,0]]

SPattern = [[0,1,1,0],
			[1,1,0,0]]

ZPattern = [[1,1,0,0],
			[0,1,1,0]]


PATTERN = {'S': SPattern,
           'Z': ZPattern,
           'J': JPattern,
           'L': LPattern,
           'I': IPattern,
           'O': OPattern,
           'T': TPattern}

# define next size
NEXT_WIDTH = 4
NEXT_HEIGHT = 2


def runGame():
	
	# create new board
	board = []
	blankline = [0,0,0,0,0,0,0,0,0,0]
	for i in range(BOARD_HEIGHT):
		board.append(blankline)

	board = np.array(board)

	rndNext = choice(list(PATTERN.keys()))
	next = PATTERN[rndNext]
	moving = False
	currentPos = []

	#game loop
	while True:
		
		for block in currentPos:
			y = block[0]
			x = block[1]
			if(isBlocked([y + 1, x], board)):
				moving = False

		if(moving):
			shiftPatternDown(currentPos, board)
		else:
			currentPos = spawnNext(next, board)
			rndNext = choice(list(PATTERN.keys()))
			next = PATTERN[rndNext]
			moving = True

		# return data to the nn
		print(board)
		print("NEXT: ", next)
		time.sleep(3)


def spawnNext(pattern, board):
	currentPos = []

	for y in range(len(pattern)):
		for x in range(len(pattern[y])):
			if(pattern[y][x] == 1):
				board[y][x] = 1
				currentPos.append([y,x])
	return currentPos

def isBlocked(block, board):
	y = block[0]
	x = block[1]
	return board[y][x] == 1

def shiftPatternDown(currentPos, board):
	for block in currentPos:
		y = block[0]
		x = block[1]
		board[y][x], board[y + 1][x] = board[y + 1][x], board[y][x] 


def shiftPatternLeft(count, currentPos, board):
	for block in currentPos:
		y = block[0]
		x = block[1]
		for left in range(count):
			left+1
			if not(x - left < 0 & isBlocked([y, x - left], board)):
				board[y][x], board[y][x - 1] = board[y][x - 1], board[y][x]

#def getInput(transforms):
	# TODO: process transformations



runGame()